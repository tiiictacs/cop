﻿namespace CustomerPortal.Services.Configuration
{
    public class SingpassAuthenticateConfiguration
    {
        public string ClientId
        {
            get; set;
        }

        public string ClientSecret
        {
            get; set;
        }

        public string Attributes
        {
            get; set;
        }

        public string AuthApiUrl
        {
            get; set;
        }

        public string TokenApiUrl
        {
            get; set;
        }

        public string PersonApiUrl
        {
            get; set;
        }

        public string RedirectUrl
        {
            get; set;
        }
        public string PublicCertificateFilename
        {
            get; set;
        }

        public string PublicKeyFilename
        {
            get; set;
        }

        public string PublicKeyPassword
        {
            get; set;
        }

        public string Purpose { get; set; }   

        public string PrivateKeyFilename { get; set; }  

        public string PrivateKeyPassword { get; set; }  

        public bool IsSandbox {  get; set; }
    }
}
