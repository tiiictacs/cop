﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerPortal.Services.Models
{
    public class ResponseModel
    {
        public ResponseModel()
        {
            ValidationMessage = new List<string>();
        }

        public int Status { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
        public List<string> ValidationMessage { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class ResponseStatusCodes
    {
        /// <summary>
        /// success code 
        /// 1. - Request servered successfully
        /// 2. - Response prepared successfully
        /// Response - Message :should be null
        /// Response - data : response object
        /// Response - status code : 1
        /// </summary>
        public const int Success = 1;

        /// <summary>
        /// Failed code
        /// 1. - Request servered successfully
        /// 2. - Exception occured
        /// Response - Message :should be Error message
        /// Response - data : null
        /// Response - status code : 0
        /// </summary>
        public const int Failed = 0;


        /// <summary>
        /// Error code
        /// 1. - Request servered successfully
        /// 2. - validation fail
        /// Response - Message : null
        /// Response - data : response object
        /// Response - status code : 2
        /// </summary>
        public const int ValidationFail = 2;
    }

    public class ResponseMessages
    {
        public const string VALIDATION_FAIL = "Parameter validation fail."; // status code = 2

    }
}
