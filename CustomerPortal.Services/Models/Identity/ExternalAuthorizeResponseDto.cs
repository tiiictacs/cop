﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerPortal.Services.Models.Identity
{
    public class ExternalAuthorizeResponseDto
    {
        public string access_token {  get; set; }
    }
}
