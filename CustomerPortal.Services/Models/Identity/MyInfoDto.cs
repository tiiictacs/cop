﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerPortal.Services.Models.Identity
{
    public class MyInfoDto
    { 
        public MyInfoUinfin uinfin {  get; set; }   

        public MyInfoName name { get; set; }    


    }

    public class MyInfoUinfin
    {
        public string source { get; set; }

        public string classification { get; set; }  

        public string value { get; set; }   
    }

    public class MyInfoName
    {
        public string source { get; set; }

        public string value { get; set; }   
    }
}
