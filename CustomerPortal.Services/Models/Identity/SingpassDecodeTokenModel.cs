﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerPortal.Services.Models.Identity
{
    public class SingpassDecodeTokenModel
    {
        public string sub {  get; set; }    

        public string jti { get; set; } 

        public List<string> scope {  get; set; }    

        public string tokenName {  get; set; }  
    }
}
