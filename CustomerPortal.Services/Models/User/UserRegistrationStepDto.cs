﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerPortal.Services.Models.User
{
    public class UserRegistrationStepDto
    {
        public long Id { get; set; }

        public string GuestId {  get; set; }  

        public string JsonData {  get; set; }   

        public int Step {  get; set; }  

        public DateTime CreatedOn { get; set; } 

        public DateTime UpdatedOn { get; set; } 
    }
}
