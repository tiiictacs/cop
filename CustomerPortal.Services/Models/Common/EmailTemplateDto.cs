﻿namespace CustomerPortal.Services.Models.Common
{
    public class EmailTemplateDto
    {
        public long Id { get; set; }

        public int Type { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public string To { get; set; }
    }
}
