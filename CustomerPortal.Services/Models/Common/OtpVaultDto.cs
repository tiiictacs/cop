﻿using CustomerPortal.DbContext.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerPortal.Services.Models.Common
{
    public class OtpVaultDto
    {
        public long Id { get; set; }

        public string Code { get; set; }

        public DateTime CreatedOn { get; set; }

        public string Email { get; set; } 

        public DateTime ExpiredOn { get; set; }

        public OtpVaultTypeEnum Type {  get; set; }    

        public string EntityID {  get; set; }
    }
}
