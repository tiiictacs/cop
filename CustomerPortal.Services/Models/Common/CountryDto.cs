﻿namespace CustomerPortal.Services.Models.Common
{
    public class CountryDto
    { 
        public long Id { get; set; }

        public string Name { get; set; }

        public string TwoLetterIsoCode { get; set; }

        public string ThreeLetterIsoCode { get; set; }

        public int NumericIsoCode { get; set; }

        public byte Published { get; set; }

        public int DisplayOrder { get; set; }
    }
}
