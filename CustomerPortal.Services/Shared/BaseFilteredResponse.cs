﻿namespace CustomerPortal.Services.Shared
{
    public abstract class BaseFilteredResponse
    {
        public int Count { get; set; }
    }
}
