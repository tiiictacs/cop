﻿using Microsoft.EntityFrameworkCore;
using CustomerPortal.DbContext.Entities;
using CustomerPortal.DbContext.Enums;
using CustomerPortal.Infrastructure.DbUtility;
using CustomerPortal.Services.Models.Common;
using CustomerPortal.Services.Services.Interfaces;
using NLog;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerPortal.Services.Services
{
    public class EmailTemplateService : IEmailTemplateService
    {
        private readonly IUnitOfWork _uow;
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public EmailTemplateService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public async Task<EmailTemplateDto> GetEmailTemplateByType(EmailTemplateType type)
        {
            try
            {
                return await _uow.Query<EmailTemplate>()
                    .Where(x => x.Type == (int)type)
                    .Select(x => new EmailTemplateDto
                    {
                        Id = x.Id,
                        Type = x.Type,
                        Subject = x.Subject,
                        Body = x.Body,
                        To = x.To
                    })
                    .AsNoTracking()
                    .FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);

                return null;
            }
        }
    }
}
