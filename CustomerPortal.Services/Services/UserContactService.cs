﻿using Microsoft.EntityFrameworkCore;
using CustomerPortal.DbContext.Entities;
using CustomerPortal.Infrastructure.DbUtility;
using CustomerPortal.Services.Logging;
using CustomerPortal.Services.Models.User;
using CustomerPortal.Services.Services.Interfaces;
using NLog;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerPortal.Services.Services
{
    public class UserContactService : IUserContactService
    {
        private readonly IUnitOfWork _uow;
        private readonly ILoggingService _logsService;
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public UserContactService(IUnitOfWork uow,
            ILoggingService logsService)
        {
            _uow = uow;
            _logsService = logsService;
        }

        public async Task<UserContactDto> AddUserContactAsync(UserContactDto dto)
        {
            try
            {
                var dbUserContact = new UserContact
                {
                    Address = dto.Address,
                    BillingAddress = dto.BillingAddress, 
                    BillingUnitNo = dto.BillingUnitNo,
                    BillingName = dto.BillingName,
                    BillingPostalCode = dto.BillingPostalCode, 
                    BRN = dto.BRN, 
                    CountryCode = dto.CountryCode,
                    Name = dto.Name,
                    PostalCode = dto.PostalCode, 
                    UEN = dto.UEN,
                    UnitNo = dto.UnitNo,
                    UserId = dto.UserId,
                    CreatedOn = DateTime.Now 
                };

                await _uow.Context.Set<UserContact>().AddAsync(dbUserContact);
                //await _logsService.SaveLogNoCommit(DateTime.Now, dto.UserId, ELogType.BlogAdded, $"New blog with url {dto.Url}."); // save log of this action
                await _uow.CommitAsync();

                dto.Id = dbUserContact.Id;

                return dto;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return null;
            }
        }

        public async Task<bool> DeleteUserContactAsync(long id)
        {
            try
            {
                var dbUserContact = _uow.Query<UserContact>(s => s.Id == id).FirstOrDefault();

                if (dbUserContact!=null)
                {
                    _uow.Context.Set<UserContact>().Remove(dbUserContact);

                    await _uow.CommitAsync();
                }
                 
                return true;
            }
            catch (Exception)
            { 
                return false;
            }
        }

        public async Task<UserContactDto> GetByUenOrBrnAsync(string searchQuery)
        {
            var query = _uow.Query<UserContact>().AsQueryable();
             
            query = query.Where(x => x.BRN.ToLower().Equals(searchQuery.ToLower()) 
                || x.UEN.ToLower().Equals(searchQuery.ToLower()));

            var business = await query.FirstOrDefaultAsync();

            if (business == null) return null;

            return new UserContactDto
            {
                Address = business.Address,
                BRN = business.BRN,  
                Name = business.Name,
                PostalCode = business.PostalCode,
                UnitNo = business.UnitNo,   
                UEN = business.UEN,  
            };
        }

        public async Task<UserContactDto> GetByUserIdAsync(long id)
        {
            var query = _uow.Query<UserContact>().AsQueryable();

            query = query.Where(x => x.UserId == id);

            var business = await query.FirstOrDefaultAsync();

            if (business == null) return null;

            return new UserContactDto
            {
                Address = business.Address,
                BRN = business.BRN,
                Name = business.Name,
                PostalCode = business.PostalCode,
                UnitNo = business.UnitNo,
                UEN = business.UEN,
                CountryCode = business.CountryCode,
                BillingUnitNo = business.BillingUnitNo,
                BillingPostalCode = business.BillingPostalCode,
                BillingName = business.BillingName,
                BillingAddress = business.BillingAddress,
                
            };
        }
    }
}
