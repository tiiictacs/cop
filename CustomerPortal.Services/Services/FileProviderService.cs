﻿using Microsoft.Extensions.FileProviders;
using CustomerPortal.Services.Services.Interfaces;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Hosting;

namespace CustomerPortal.Services.Services
{
    public class FileProviderService : PhysicalFileProvider, IFileProviderService
    {
        public FileProviderService(IWebHostEnvironment hostingEnvironment)
            : base(File.Exists(hostingEnvironment.ContentRootPath) ? Path.GetDirectoryName(hostingEnvironment.ContentRootPath) : hostingEnvironment.ContentRootPath)
        {
            var path = hostingEnvironment.ContentRootPath ?? string.Empty;
            if (File.Exists(path))
                path = Path.GetDirectoryName(path);

            BaseDirectory = path;
        }

        protected string BaseDirectory { get; }

        public string GetAbsolutePath(params string[] paths)
        {
            var allPaths = paths.ToList();
            allPaths.Insert(0, Root);

            return Path.Combine(allPaths.ToArray());
        }
    }
}
