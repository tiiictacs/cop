﻿using Microsoft.EntityFrameworkCore;
using CustomerPortal.DbContext.Entities;
using CustomerPortal.DbContext.Enums;
using CustomerPortal.Infrastructure.DbUtility;
using CustomerPortal.Services.Logging;
using CustomerPortal.Services.Models;
using CustomerPortal.Services.Models.Common;
using CustomerPortal.Services.Services.Interfaces;
using CustomerPortal.Utility.Helpers;
using NLog;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerPortal.Services.Services
{
    public class OtpService: IOtpService
    {
        private readonly IUnitOfWork _uow;
        private readonly ILoggingService _logsService;
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public OtpService(IUnitOfWork uow,
            ILoggingService logsService)
        {
            _uow = uow;
            _logsService = logsService;
        }

        public async Task<GenericResult> GenerateEmailVerificationOtp(string email)
        {
            var code = TextHelper.RandomNumber(6);

            var dto =  await AddOtpVault(new OtpVaultDto
            {
                Code = code,
                CreatedOn = DateTime.Now,
                Email = email,
                ExpiredOn = DateTime.Now.AddSeconds(90),
                Type =  OtpVaultTypeEnum.EmailVerification,
                EntityID = string.Empty
            });

            if (dto==null) return new GenericResult(false, "Error, retry later");

            return new GenericResult(true, code);

        }

        public async Task<GenericResult> GenerateProductVerificationOtp(string email,string entityID)
        {
            var code = TextHelper.RandomNumber(6);

            var dto = await AddOtpVault(new OtpVaultDto
            {
                Code = code,
                CreatedOn = DateTime.Now,
                Email = email,
                ExpiredOn = DateTime.Now.AddHours(24),
                Type = OtpVaultTypeEnum.ProductVerification,
                EntityID = entityID
            });

            if (dto == null) return new GenericResult(false, "Error, retry later");

            return new GenericResult(true, code);

        }

        public async Task<GenericResult> VerifyOtp(string email,string otp)
        {
            var otpDb = await GetLatestOtpByEmail(email, OtpVaultTypeEnum.EmailVerification);

            if (otpDb==null) return new GenericResult(false,"OTP invalid");

            if (otpDb.ExpiredOn<=DateTime.Now) return new GenericResult(false, "OTP expired");

            if (!otpDb.Code.Equals(otp)) return new GenericResult(false, "OTP invalid");

            return new GenericResult(true, "");
        }

        public async Task<GenericResult> VerifyProductOtpAsync(string email, string otp,string entityID)
        {
            var otpDb = await GetLatestOtpByEmailAndId(email, OtpVaultTypeEnum.ProductVerification, entityID);

            if (otpDb == null) return new GenericResult(false, "OTP invalid");

            if (otpDb.ExpiredOn <= DateTime.Now) return new GenericResult(false, "OTP expired");

            if (!otpDb.Code.Equals(otp)) return new GenericResult(false, "OTP invalid");

            return new GenericResult(true, "");
        }

        public async Task<OtpVaultDto> AddOtpVault(OtpVaultDto dto)
        {
            try
            {
                var dbOtpVault = new OtpVault
                {
                    Code = dto.Code,
                    CreatedOn = dto.CreatedOn,
                    Email = dto.Email,
                    ExpiredOn = dto.ExpiredOn, 
                    EntityID = dto.EntityID,
                    Type = dto.Type,

                };

                await _uow.Context.Set<OtpVault>().AddAsync(dbOtpVault);
                //await _logsService.SaveLogNoCommit(DateTime.Now, ELogType.OtpVaultAdded, $"New otp with email {dto.Email}."); // save log of this action
                await _uow.CommitAsync();

                dto.Id = dbOtpVault.Id;

                return dto;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return null;
            }
        }

        async Task<OtpVault> GetLatestOtpByEmail(string email , OtpVaultTypeEnum type)
        {
            var otp = await _uow.Query<OtpVault>(s => s.Email == email && s.Type == type)
                .OrderByDescending(x=>x.Id)
                .FirstOrDefaultAsync();

            return otp;
        }

        async Task<OtpVault> GetLatestOtpByEmailAndId(string email, OtpVaultTypeEnum type, string entityID)
        {
            var otp = await _uow.Query<OtpVault>(s => s.Email == email && s.Type == type && s.EntityID == entityID)
                .OrderByDescending(x => x.Id)
                .FirstOrDefaultAsync();

            return otp;
        }
    }
}
