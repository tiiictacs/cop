﻿using Microsoft.EntityFrameworkCore;
using CustomerPortal.DbContext.Entities;
using CustomerPortal.DbContext.Enums;
using CustomerPortal.Infrastructure.DbUtility;
using CustomerPortal.Services.Logging;
using CustomerPortal.Services.Models.Product;
using CustomerPortal.Services.Services.Interfaces;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerPortal.Services.Services
{
    public class ProductService :IProductService
    {
        private readonly IUnitOfWork _uow;
        private readonly ILoggingService _logsService;
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public ProductService(IUnitOfWork uow,
            ILoggingService logsService)
        {
            _uow = uow;
            _logsService = logsService;
        }

        public async Task<List<ProductDto>> GetProducts()
        {
            try
            {
                var products = await _uow.Query<Product>()
                                      .AsNoTracking()
                                      .ToListAsync();
                if (!products.Any())
                    return null;

                return products.Select(s => new ProductDto
                {
                    Id = s.Id,
                    Name = s.Name,
                    Description = s.Description,
                    GroupName = s.GroupName,
                    
                }).ToList();
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return null;
            }
        }

        public async Task<bool> AddProductMappings(List<long> productIds, long userId)
        {
            try
            {
                foreach (var productId in productIds)
                {
                    await _uow.Context.Set<UserProductMapping>().AddAsync(new UserProductMapping
                    {
                        UserId = userId,
                        ProductId = productId,
                        CreatedOn = DateTime.Now,
                        Status = ProductMappingStatusEnum.InActive
                        
                    });
                }

                await _uow.CommitAsync();

                return true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
            }
            
        }

        public async Task<bool> DeleteProductMappingsByUserId(long userId)
        {
            try
            {
                var dbProductMappings = _uow.Query<UserProductMapping>(s => s.UserId == userId).ToList();

                foreach (var item in dbProductMappings)
                {
                    _uow.Context.Set<UserProductMapping>().Remove(item);

                    await _uow.CommitAsync();
                } 
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> VerificationUserProduct(long productId, long userId)
        { 
            try
            {
                var productMapping = await _uow.Query<UserProductMapping>(x => x.ProductId == productId && x.UserId == userId)
                                      .FirstOrDefaultAsync();

                if (productMapping == null) return false;

                if (productMapping.Status == ProductMappingStatusEnum.Active)
                    return true;

                productMapping.ActivatedOn = DateTime.Now;
                productMapping.Status = ProductMappingStatusEnum.Active;
                 
                await _uow.CommitAsync();

                return true;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return false;
            }
             
        }

        public async Task<bool> AddUserProductMappingContact(long productId,
            long userId,
            string name,
            string phoneNumber,
            string companyEmail,
            string companyName)
        {
            try
            {
                var productMapping = await _uow.Query<UserProductMapping>(x => x.ProductId == productId && x.UserId == userId)
                                      .FirstOrDefaultAsync();

                if (productMapping == null) return false;

                if (productMapping.Status == ProductMappingStatusEnum.InActive)
                    return false;

                productMapping.CompanyEmail = companyEmail;
                productMapping.CompanyName = companyName;
                productMapping.PhoneNumber = phoneNumber;
                productMapping.Name = name;

                await _uow.CommitAsync();

                return true;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return false;
            }

        }

        public async Task<ProductDto> GetByIdAsync(long id)
        {
            try
            {
                var product = await _uow.Query<Product>()
                                      .AsNoTracking()
                                      .FirstOrDefaultAsync();
                if (product == null)
                    return null;

                return new ProductDto
                {
                    Id = product.Id,
                    Name = product.Name,
                    Description = product.Description,
                    GroupName = product.GroupName,

                };
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return null;
            }
        }

        public async Task<List<long>> GetProductIdsByUserIdAsync(long userId)
        {
            try
            {
                var productIds = await _uow.Query<UserProductMapping>() 
                                      .Where(x=>x.UserId == userId).Select(x=>x.ProductId).ToListAsync();
                return productIds;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return null;
            }
        }
    }
}
