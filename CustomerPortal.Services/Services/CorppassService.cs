﻿using Microsoft.Extensions.Configuration;
using CustomerPortal.Services.Configuration;
using CustomerPortal.Services.Models.Identity;
using CustomerPortal.Services.Services.Interfaces;
using Newtonsoft.Json;
using System;
using CustomerPortal.Utility.Helpers;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using CustomerPortal.Services.Models;

namespace CustomerPortal.Services.Services
{
    public class CorppassService: ICorppassService
    {
        protected readonly IConfiguration _configuration;
        protected readonly IHttpClientService _httpClientService;
        protected readonly IFileProviderService _fileProviderService;


        public CorppassService(IConfiguration configuration,
            IHttpClientService httpClientService,
            IFileProviderService fileProviderService)
        { 
            _httpClientService = httpClientService;
            _configuration = configuration;
            _fileProviderService = fileProviderService;
        }

        private CorppassAuthenticateConfiguration CorppassAuthenticateConfiguration
        {
            get
            {
                return _configuration.GetSection(nameof(CorppassAuthenticateConfiguration)).Get<CorppassAuthenticateConfiguration>();
            }
        }


        public string GetAuthoriseUrl()
        {
            var authoriseUrl = CorppassAuthenticateConfiguration.AuthApiUrl +
            "?client_id=" + CorppassAuthenticateConfiguration.ClientId +
            "&attributes=" + CorppassAuthenticateConfiguration.Attributes +
            "&purpose=" + CorppassAuthenticateConfiguration.Purpose +
            "&state=" + "singpass" +
            "&redirect_uri=" + CorppassAuthenticateConfiguration.RedirectUrl;

            return authoriseUrl;    
        }

        public MyInfoDto GetMyInfo(string code)
        {
            var accessToken =  GetAccessToken(code);

            if (string.IsNullOrEmpty(accessToken))
                return null;

            var decodeData = JsonConvert.DeserializeObject<SingpassDecodeTokenModel>(MyInfoSecurityHelper.DecodeToken(accessToken).ToString());

            var arr = decodeData.sub.Split("_");
            var uen = arr[0];
            var uuid = arr[1];

            var myInfoEnc = GetMyInfoData(uen, uuid, $"Bearer {accessToken}", string.Empty);

            if (!myInfoEnc.Success)
                return null;

            string jsonStr = string.Empty;

            if (CorppassAuthenticateConfiguration.IsSandbox)
            {
                jsonStr = myInfoEnc.Message; 
            }
            else
            {
                jsonStr = DecodeTokenToMyInfoData(myInfoEnc.Message);
            }

            var tmpInfo = JsonConvert.DeserializeObject<CorppassMyInfoDto>(jsonStr);
                 


            return tmpInfo.person;
        }

        protected string GetAccessToken(string authCode)
        {
            string baseParams;
            string accessToken = string.Empty ;
            string baseString = string.Empty;
            string authHeader = string.Empty;

            try
            {
                var nonce = MyInfoSecurityHelper.GetRandomInteger();
                long timestamp = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

                string signature = null;

                // A) Forming the Signature Base String
                baseParams = $"app_id={CorppassAuthenticateConfiguration.ClientId}" +
                    $"&client_id={CorppassAuthenticateConfiguration.ClientId}" +
                    $"&client_secret={CorppassAuthenticateConfiguration.ClientSecret}" +
                    $"&code={authCode}" +
                    $"&grant_type=authorization_code" +
                    $"&nonce={nonce}" +
                    $"&redirect_uri={CorppassAuthenticateConfiguration.RedirectUrl}" +
                    $"&signature_method=RS256" +
                    $"&state={string.Empty}" +
                    $"&timestamp={timestamp}";
                baseString = MyInfoSecurityHelper.GenerateBaseString("POST", CorppassAuthenticateConfiguration.TokenApiUrl, baseParams);

                // B) Signing Base String to get Digital Signature
                if (baseString != null)
                {
                    signature = MyInfoSecurityHelper.GenerateSignature(baseString, GetPrivateKey().ToXmlString(true));
                }

                // C) Assembling the Header
                if (signature != null)
                {
                    string headers = $"{ApplicationConstant.APP_ID}=\"{CorppassAuthenticateConfiguration.ClientId}\"," +
                        $"{ApplicationConstant.NONCE}=\"{nonce}\"," +
                        $"{ApplicationConstant.SIGNATURE_METHOD}=\"{ApplicationConstant.RS256}\"," +
                        $"{ApplicationConstant.SIGNATURE}=\"{signature}\"," +
                        $"{ApplicationConstant.TIMESTAMP}=\"{timestamp}\"";
                    authHeader = MyInfoSecurityHelper.GenerateAuthorizationHeader(headers, null);
                }

                // D) Assembling the params
                string parameters = $"{ApplicationConstant.GRANT_TYPE}={ApplicationConstant.AUTHORIZATION_CODE}" +
                    $"&{ApplicationConstant.CODE}={authCode}" +
                    $"&{ApplicationConstant.REDIRECT_URI}={CorppassAuthenticateConfiguration.RedirectUrl}" +
                    $"&{ApplicationConstant.CLIENT_ID}={CorppassAuthenticateConfiguration.ClientId}" +
                    $"&{ApplicationConstant.CLIENT_SECRET}={CorppassAuthenticateConfiguration.ClientSecret}" +
                    $"&{ApplicationConstant.STATE}={""}";

                // E) Prepare request for TOKEN API

                var response = _httpClientService.PostSingpass(CorppassAuthenticateConfiguration.TokenApiUrl, parameters, authHeader);

                if (!response.Success) { return string.Empty; }

                var accessTokenModel = JsonConvert.DeserializeObject<ExternalAuthorizeResponseDto>(response.Message);
                 
                if (accessTokenModel != null) accessToken = accessTokenModel.access_token;

            }
            catch (Exception ex)
            {
                var sgLocal = DateTimeOffset.UtcNow.ToOffset(TimeSpan.FromHours(8));
                throw new Exception($@"Request for AccessToken rejected. Support template data:
                        Time='{sgLocal}'
                        GET='{CorppassAuthenticateConfiguration.TokenApiUrl}'
                        BaseString='{baseString}'
                        AuthHeader='{authHeader}'
                        AuthCode='{authCode}'
                        State='{""}'"
                , ex);
            }

            return accessToken;
        }

        private X509Certificate2 _x509private;
        private X509Certificate2 _x509public;

        internal RSA GetPrivateKey()
        {


            if (_x509private == null)
            { 
                var filePath = _fileProviderService.GetAbsolutePath(CorppassAuthenticateConfiguration.PrivateKeyFilename);
                _x509private = new X509Certificate2(filePath, CorppassAuthenticateConfiguration.PrivateKeyPassword, X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.Exportable);
            }
            var privateKey = _x509private.GetRSAPrivateKey();

            return privateKey;
        }

        internal RSA GetPublicKey()
        {
            if (_x509public == null)
            {
                var filePath = _fileProviderService.GetAbsolutePath(CorppassAuthenticateConfiguration.PublicCertificateFilename);
                _x509public = new X509Certificate2(filePath);
            }
            var publicKey = _x509public.GetRSAPublicKey();
            return publicKey;
        }

        GenericResult GetMyInfoData(string uen, string uuid, string bearer, string txnNo)
        {
            string baseParams; 
            var Client_Id = CorppassAuthenticateConfiguration.ClientId; 
            var PersonApi_Url = CorppassAuthenticateConfiguration.PersonApiUrl;
            var attribute = CorppassAuthenticateConfiguration.Attributes;
            try
            {
                var specificPersonUrl = $"{PersonApi_Url}/{uen}/{uuid}";

                var nonce = MyInfoSecurityHelper.GetRandomInteger();
                long timestamp = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

                string signature = null;
                string authHeader = null;

                // A) Forming the Signature Base String
                baseParams = $"{ApplicationConstant.APP_ID}={Client_Id}&{ApplicationConstant.ATTRIBUTE}={attribute}&{ApplicationConstant.CLIENT_ID}={Client_Id}&{ApplicationConstant.NONCE}={nonce}&{ApplicationConstant.SIGNATURE_METHOD}={ApplicationConstant.RS256}&{ApplicationConstant.TIMESTAMP}={timestamp}";

                if (txnNo != null)
                {
                    baseParams = $"{baseParams}&{ApplicationConstant.TRANSACTION_NO}={txnNo}";
                }
                string baseString = MyInfoSecurityHelper.GenerateBaseString(ApplicationConstant.GET_METHOD, specificPersonUrl, baseParams);

                // B) Signing Base String to get Digital Signature
                if (baseString != null)
                {
                    var privateKey = GetPrivateKey().ToXmlString(true);
                    signature = MyInfoSecurityHelper.GenerateSignature(baseString, privateKey);
                }

                // C) Assembling the Header
                if (signature != null)
                {
                    string header = $"{ApplicationConstant.APP_ID}=\"{Client_Id}\",{ApplicationConstant.NONCE}=\"{nonce}\",{ApplicationConstant.SIGNATURE_METHOD}=\"{ApplicationConstant.RS256}\",{ApplicationConstant.SIGNATURE}=\"{signature}\",{ApplicationConstant.TIMESTAMP}=\"{timestamp}\"";
                    authHeader = MyInfoSecurityHelper.GenerateAuthorizationHeader(header, bearer);
                }

                // D) Assembling the params
                specificPersonUrl = $"{specificPersonUrl}?{ApplicationConstant.CLIENT_ID}={Client_Id}&{ApplicationConstant.ATTRIBUTE}={attribute}";

                if (txnNo != null)
                {
                    specificPersonUrl = $"{specificPersonUrl}&{ApplicationConstant.TRANSACTION_NO}={txnNo}";
                }

                var myInfo = _httpClientService.GetSingpass(specificPersonUrl, authHeader);

                return myInfo;
            }
            catch (Exception ex)
            {
                 
            }

            return new GenericResult(false,"ERROR");
        }

        string DecodeTokenToMyInfoData(string encryptedToken)
        {
            string decodedJson = string.Empty;

            // Decrypt
            var privateKey = GetPrivateKey();
            string plainToken = Jose.JWT.Decode(encryptedToken, privateKey);

            // Verify
            var publicKey = GetPublicKey();

            if (MyInfoSecurityHelper.VerifyToken(plainToken, publicKey))
            {
                var jsonObject = MyInfoSecurityHelper.DecodeToken(plainToken);
                decodedJson = jsonObject.ToString();
            }
            else
            {
                Console.WriteLine($"{nameof(DecodeTokenToMyInfoData)} Failed to verify using MyInfo's public certificate. Call MyInfoConnectorConfig.GetCertificateInfo() to confirm certificate details");
            }

            return decodedJson;
        }
    }
}
