﻿using CustomerPortal.DbContext.Entities;
using CustomerPortal.Infrastructure.DbUtility;
using CustomerPortal.Services.Logging;
using CustomerPortal.Services.Models.User;
using CustomerPortal.Services.Services.Interfaces;
using NLog;
using System.Threading.Tasks;
using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace CustomerPortal.Services.Services
{
    public class UserRegistrationStepsService : IUserRegistrationStepsService
    {
        private readonly IUnitOfWork _uow;
        private readonly ILoggingService _logsService;
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public UserRegistrationStepsService(IUnitOfWork uow,
            ILoggingService logsService)
        {
            _uow = uow;
            _logsService = logsService;
        }

        public async Task<UserRegistrationStepDto> AddUserRegistrationStepAsync(UserRegistrationStepDto dto)
        {
            try
            {
                var dbUserRegistrationStep = new UserRegistrationStep
                {
                     
                    CreatedOn = DateTime.Now,
                    JsonData = dto.JsonData,
                    Step = dto.Step,
                    UpdatedOn = DateTime.Now,
                    GuestId = dto.GuestId,
                };

                await _uow.Context.Set<UserRegistrationStep>().AddAsync(dbUserRegistrationStep);
                //await _logsService.SaveLogNoCommit(DateTime.Now, dto.UserId, ELogType.BlogAdded, $"New blog with url {dto.Url}."); // save log of this action
                await _uow.CommitAsync();

                dto.Id = dbUserRegistrationStep.Id;

                return dto;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return null;
            }
        }

        public async Task<bool> DeleteUserRegistrationStepAsync(long id)
        {
            try
            {
                var dbUserRegistrationStep = _uow.Query<UserRegistrationStep>(s => s.Id == id).FirstOrDefault();

                if (dbUserRegistrationStep != null)
                {
                    _uow.Context.Set<UserRegistrationStep>().Remove(dbUserRegistrationStep);

                    await _uow.CommitAsync();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> DeleteUserRegistrationStepAsync(string guestId)
        {
            try
            {
                var dbUserRegistrationSteps = _uow.Query<UserRegistrationStep>(s => s.GuestId == guestId).ToList();

                foreach (var step in dbUserRegistrationSteps)
                {
                    _uow.Context.Set<UserRegistrationStep>().Remove(step);

                    
                }

                await _uow.CommitAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<UserRegistrationStepDto> GetByGuestIdAndStepAsync(string guestId,int step)
        {
            var query = _uow.Query<UserRegistrationStep>().AsQueryable();

            query = query.Where(x => x.GuestId == guestId && x.Step == step);

            var userRegistrationStep = await query.FirstOrDefaultAsync();

            if (userRegistrationStep == null) return null;

            return new UserRegistrationStepDto
            {
                Step = step,
                JsonData = userRegistrationStep.JsonData,
                GuestId = userRegistrationStep.GuestId, 
                CreatedOn = userRegistrationStep.CreatedOn,
                Id = userRegistrationStep.Id
            };
        }
    }
}
