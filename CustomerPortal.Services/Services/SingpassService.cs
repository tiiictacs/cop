﻿using Microsoft.Extensions.Configuration;
using CustomerPortal.Services.Configuration;
using CustomerPortal.Services.Models.Identity;
using CustomerPortal.Services.Services.Interfaces;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Text;
using System;
using System.Threading.Tasks;
using CustomerPortal.Utility.Helpers;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using CustomerPortal.Services.Models;
using System.Text.Json.Serialization;

namespace CustomerPortal.Services.Services
{
    public class SingpassService: ISingpassService
    {
        protected readonly IConfiguration _configuration;
        protected readonly IHttpClientService _httpClientService;
        protected readonly IFileProviderService _fileProviderService;


        public SingpassService(IConfiguration configuration,
            IHttpClientService httpClientService,
            IFileProviderService fileProviderService)
        { 
            _httpClientService = httpClientService;
            _configuration = configuration;
            _fileProviderService = fileProviderService;
        }

        private SingpassAuthenticateConfiguration SingpassAuthenticateConfiguration
        {
            get
            {
                return _configuration.GetSection(nameof(SingpassAuthenticateConfiguration)).Get<SingpassAuthenticateConfiguration>();
            }
        }


        public string GetAuthoriseUrl()
        {
            var authoriseUrl = SingpassAuthenticateConfiguration.AuthApiUrl +
            "?client_id=" + SingpassAuthenticateConfiguration.ClientId +
            "&attributes=" + SingpassAuthenticateConfiguration.Attributes +
            "&purpose=" + SingpassAuthenticateConfiguration.Purpose +
            "&state=" + "singpass" +
            "&redirect_uri=" + SingpassAuthenticateConfiguration.RedirectUrl;

            return authoriseUrl;    
        }

        public MyInfoDto GetMyInfo(string code)
        {
            var accessToken =  GetAccessToken(code);

            if (string.IsNullOrEmpty(accessToken))
                return null;

            var decodeData = JsonConvert.DeserializeObject<SingpassDecodeTokenModel>(MyInfoSecurityHelper.DecodeToken(accessToken).ToString());

            var myInfoEnc = GetMyInfoData(decodeData.sub, $"Bearer {accessToken}", string.Empty);

            if (!myInfoEnc.Success)
                return null;
             
            if (SingpassAuthenticateConfiguration.IsSandbox)
                return JsonConvert.DeserializeObject<MyInfoDto>(myInfoEnc.Message);

            var myInfoDecrypt = DecodeTokenToMyInfoData(myInfoEnc.Message);
             
            return JsonConvert.DeserializeObject<MyInfoDto>(myInfoDecrypt);
        }

        protected string GetAccessToken(string authCode)
        {
            string baseParams;
            string accessToken = string.Empty ;
            string baseString = string.Empty;
            string authHeader = string.Empty;

            try
            {
                var nonce = MyInfoSecurityHelper.GetRandomInteger();
                long timestamp = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

                string signature = null;

                // A) Forming the Signature Base String
                baseParams = $"app_id={SingpassAuthenticateConfiguration.ClientId}" +
                    $"&client_id={SingpassAuthenticateConfiguration.ClientId}" +
                    $"&client_secret={SingpassAuthenticateConfiguration.ClientSecret}" +
                    $"&code={authCode}" +
                    $"&grant_type=authorization_code" +
                    $"&nonce={nonce}" +
                    $"&redirect_uri={SingpassAuthenticateConfiguration.RedirectUrl}" +
                    $"&signature_method=RS256" +
                    $"&state={string.Empty}" +
                    $"&timestamp={timestamp}";
                baseString = MyInfoSecurityHelper.GenerateBaseString("POST", SingpassAuthenticateConfiguration.TokenApiUrl, baseParams);

                // B) Signing Base String to get Digital Signature
                if (baseString != null)
                {
                    signature = MyInfoSecurityHelper.GenerateSignature(baseString, GetPrivateKey().ToXmlString(true));
                }

                // C) Assembling the Header
                if (signature != null)
                {
                    string headers = $"{ApplicationConstant.APP_ID}=\"{SingpassAuthenticateConfiguration.ClientId}\"," +
                        $"{ApplicationConstant.NONCE}=\"{nonce}\"," +
                        $"{ApplicationConstant.SIGNATURE_METHOD}=\"{ApplicationConstant.RS256}\"," +
                        $"{ApplicationConstant.SIGNATURE}=\"{signature}\"," +
                        $"{ApplicationConstant.TIMESTAMP}=\"{timestamp}\"";
                    authHeader = MyInfoSecurityHelper.GenerateAuthorizationHeader(headers, null);
                }

                // D) Assembling the params
                string parameters = $"{ApplicationConstant.GRANT_TYPE}={ApplicationConstant.AUTHORIZATION_CODE}" +
                    $"&{ApplicationConstant.CODE}={authCode}" +
                    $"&{ApplicationConstant.REDIRECT_URI}={SingpassAuthenticateConfiguration.RedirectUrl}" +
                    $"&{ApplicationConstant.CLIENT_ID}={SingpassAuthenticateConfiguration.ClientId}" +
                    $"&{ApplicationConstant.CLIENT_SECRET}={SingpassAuthenticateConfiguration.ClientSecret}" +
                    $"&{ApplicationConstant.STATE}={""}";

                // E) Prepare request for TOKEN API

                var response = _httpClientService.PostSingpass(SingpassAuthenticateConfiguration.TokenApiUrl, parameters, authHeader);

                if (!response.Success) { return string.Empty; }

                var accessTokenModel = JsonConvert.DeserializeObject<ExternalAuthorizeResponseDto>(response.Message);
                 
                if (accessTokenModel != null) accessToken = accessTokenModel.access_token;

            }
            catch (Exception ex)
            {
                var sgLocal = DateTimeOffset.UtcNow.ToOffset(TimeSpan.FromHours(8));
                throw new Exception($@"Request for AccessToken rejected. Support template data:
                        Time='{sgLocal}'
                        GET='{SingpassAuthenticateConfiguration.TokenApiUrl}'
                        BaseString='{baseString}'
                        AuthHeader='{authHeader}'
                        AuthCode='{authCode}'
                        State='{""}'"
                , ex);
            }

            return accessToken;
        }

        private X509Certificate2 _x509private;
        private X509Certificate2 _x509public;

        internal RSA GetPrivateKey()
        {


            if (_x509private == null)
            { 
                var filePath = _fileProviderService.GetAbsolutePath(SingpassAuthenticateConfiguration.PrivateKeyFilename);
                _x509private = new X509Certificate2(filePath, SingpassAuthenticateConfiguration.PrivateKeyPassword, X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.Exportable);
            }
            var privateKey = _x509private.GetRSAPrivateKey();

            return privateKey;
        }

        internal RSA GetPublicKey()
        {
            if (_x509public == null)
            {
                var filePath = _fileProviderService.GetAbsolutePath(SingpassAuthenticateConfiguration.PublicCertificateFilename);
                _x509public = new X509Certificate2(filePath);
            }
            var publicKey = _x509public.GetRSAPublicKey();
            return publicKey;
        }

        GenericResult GetMyInfoData(string uinFin, string bearer, string txnNo)
        {
            string baseParams; 
            var Client_Id = SingpassAuthenticateConfiguration.ClientId; 
            var PersonApi_Url = SingpassAuthenticateConfiguration.PersonApiUrl;
            var attribute = SingpassAuthenticateConfiguration.Attributes;
            try
            {
                var specificPersonUrl = $"{PersonApi_Url}/{uinFin}/";

                var nonce = MyInfoSecurityHelper.GetRandomInteger();
                long timestamp = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

                string signature = null;
                string authHeader = null;

                // A) Forming the Signature Base String
                baseParams = $"{ApplicationConstant.APP_ID}={Client_Id}&{ApplicationConstant.ATTRIBUTE}={attribute}&{ApplicationConstant.CLIENT_ID}={Client_Id}&{ApplicationConstant.NONCE}={nonce}&{ApplicationConstant.SIGNATURE_METHOD}={ApplicationConstant.RS256}&{ApplicationConstant.TIMESTAMP}={timestamp}";

                if (txnNo != null)
                {
                    baseParams = $"{baseParams}&{ApplicationConstant.TRANSACTION_NO}={txnNo}";
                }
                string baseString = MyInfoSecurityHelper.GenerateBaseString(ApplicationConstant.GET_METHOD, specificPersonUrl, baseParams);

                // B) Signing Base String to get Digital Signature
                if (baseString != null)
                {
                    var privateKey = GetPrivateKey().ToXmlString(true);
                    signature = MyInfoSecurityHelper.GenerateSignature(baseString, privateKey);
                }

                // C) Assembling the Header
                if (signature != null)
                {
                    string header = $"{ApplicationConstant.APP_ID}=\"{Client_Id}\",{ApplicationConstant.NONCE}=\"{nonce}\",{ApplicationConstant.SIGNATURE_METHOD}=\"{ApplicationConstant.RS256}\",{ApplicationConstant.SIGNATURE}=\"{signature}\",{ApplicationConstant.TIMESTAMP}=\"{timestamp}\"";
                    authHeader = MyInfoSecurityHelper.GenerateAuthorizationHeader(header, bearer);
                }

                // D) Assembling the params
                specificPersonUrl = $"{specificPersonUrl}?{ApplicationConstant.CLIENT_ID}={Client_Id}&{ApplicationConstant.ATTRIBUTE}={attribute}";

                if (txnNo != null)
                {
                    specificPersonUrl = $"{specificPersonUrl}&{ApplicationConstant.TRANSACTION_NO}={txnNo}";
                }

                var myInfo = _httpClientService.GetSingpass(specificPersonUrl, authHeader);

                return myInfo;
            }
            catch (Exception ex)
            {
                 
            }

            return new GenericResult(false,"ERROR");
        }

        string DecodeTokenToMyInfoData(string encryptedToken)
        {
            string decodedJson = string.Empty;

            // Decrypt
            var privateKey = GetPrivateKey();
            string plainToken = Jose.JWT.Decode(encryptedToken, privateKey);

            // Verify
            var publicKey = GetPublicKey();

            if (MyInfoSecurityHelper.VerifyToken(plainToken, publicKey))
            {
                var jsonObject = MyInfoSecurityHelper.DecodeToken(plainToken);
                decodedJson = jsonObject.ToString();
            }
            else
            {
                Console.WriteLine($"{nameof(DecodeTokenToMyInfoData)} Failed to verify using MyInfo's public certificate. Call MyInfoConnectorConfig.GetCertificateInfo() to confirm certificate details");
            }

            return decodedJson;
        }
    }
}
