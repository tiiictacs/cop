﻿using Azure.Core;
using CustomerPortal.Services.Configuration;
using CustomerPortal.Services.Models;
using CustomerPortal.Services.Services.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CustomerPortal.Services.Services
{
    public class HttpClientService:IHttpClientService
    {

        public HttpClientService() { }


        public GenericResult PostSingpass(string url, object parameters, string authHeader)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = ApplicationConstant.POST_METHOD;
                request.ContentType = "application/x-www-form-urlencoded";
                request.Headers.Add(ApplicationConstant.CACHE_CONTROL, ApplicationConstant.NO_CACHE);
                if (!string.IsNullOrEmpty(authHeader))
                {
                    request.Headers.Add(ApplicationConstant.AUTHORIZATION, authHeader);
                }

                byte[] byteArray = Encoding.UTF8.GetBytes(parameters.ToString());
                request.ContentLength = byteArray.Length;
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                request.Accept = "application/json";

                var response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var contentResponse = new StreamReader(response.GetResponseStream()).ReadToEnd();
                    return new GenericResult(true, message: contentResponse);
                }
                else
                {
                    return new GenericResult(false, message: response.StatusCode.ToString());
                }

            }
            catch (Exception ex)
            {
                //_logger.LogInformation("HttpService_PostAsync_Url: {0}", url);
                //_logger.LogError("HttpService_PostAsync_Error: {0}", ex.Message);

                return new GenericResult(false, message: ex.Message);
            }
        }

        public GenericResult GetSingpass(string url, string authHeader)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);

                request.Headers.Add(ApplicationConstant.CACHE_CONTROL, ApplicationConstant.NO_CACHE);

                request.Method = ApplicationConstant.GET_METHOD;
                request.Headers.Add("Authorization", authHeader);
                var response = (HttpWebResponse)request.GetResponse();
                Stream stream = null;
                using (stream = response.GetResponseStream())
                { 
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        using var sr = new StreamReader(stream);
                        var content = sr.ReadToEnd();
                        return new GenericResult(true, message: content);
                    }
                    else
                        return new GenericResult(false, message: response.StatusCode.ToString());
                } 
            }
            catch (Exception ex)
            {
                //_logger.LogInformation("HttpService_PostAsync_Url: {0}", url);
                //_logger.LogError("HttpService_PostAsync_Error: {0}", ex.Message);

                return new GenericResult(false, message: ex.Message);
            }
        }
    }
}
