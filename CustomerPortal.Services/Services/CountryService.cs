﻿using Microsoft.EntityFrameworkCore;
using CustomerPortal.Infrastructure.DbUtility;
using CustomerPortal.Services.Logging;
using CustomerPortal.Services.Models.Common;
using CustomerPortal.Services.Services.Interfaces;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerPortal.Services.Services
{
    public class CountryService : ICountryService
    {
        private readonly IUnitOfWork _uow;
        private readonly ILoggingService _logsService;
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public CountryService(IUnitOfWork uow,
            ILoggingService logsService)
        {
            _uow = uow;
            _logsService = logsService;
        }

        public async Task<List<CountryDto>> GetCountries()
        {
            try
            {
                var countries = await _uow.Query<DbContext.Entities.Country>()
                                      .AsNoTracking()
                                      .Where(x=>x.Published==1)
                                      .ToListAsync();
                if (!countries.Any())
                    return null;

                return countries.Select(s => new CountryDto { Id = s.Id, 
                    Name = s.Name,
                    NumericIsoCode = s.NumericIsoCode,
                    DisplayOrder = s.DisplayOrder,
                    Published = s.Published,
                    ThreeLetterIsoCode = s.ThreeLetterIsoCode,
                    TwoLetterIsoCode = s.TwoLetterIsoCode
                }).ToList();
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return null;
            }
        }
         
    }
}
