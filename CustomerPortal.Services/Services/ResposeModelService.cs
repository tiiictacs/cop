﻿using CustomerPortal.Services.Models;
using CustomerPortal.Services.Services.Interfaces;
using System.Collections.Generic; 
namespace CustomerPortal.Services.Services
{
    public class ResposeModelService : IResposeModelService
    {
        public ResponseModel GetRespons(int status, string message, object data = null, List<string> validationMessage = null, string errorMessage = null)
        { 
           var model = new ResponseModel
            {
                Status = status,
                Message = message,
                Data = data
            };
            if (validationMessage != null)
                model.ValidationMessage = validationMessage;
            if (errorMessage != null)
                model.ErrorMessage = errorMessage;
            return model;
        }
    }
}
