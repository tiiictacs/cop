﻿using CustomerPortal.DbContext.Enums;
using CustomerPortal.Infrastructure.Services;
using CustomerPortal.Services.Models.Common;
using System.Threading.Tasks;

namespace CustomerPortal.Services.Services.Interfaces
{
    public interface IEmailTemplateService : IService
    {
        Task<EmailTemplateDto> GetEmailTemplateByType(EmailTemplateType type);
    }
}
