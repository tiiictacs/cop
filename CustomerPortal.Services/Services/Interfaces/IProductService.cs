﻿using CustomerPortal.Infrastructure.Services;
using CustomerPortal.Services.Models.Product;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CustomerPortal.Services.Services.Interfaces
{
    public interface IProductService:IService
    {
        Task<List<ProductDto>> GetProducts();

        Task<bool> AddProductMappings(List<long> productIds, long userId);

        Task<bool> VerificationUserProduct(long productId, long userId);

        Task<ProductDto> GetByIdAsync(long id);

        Task<bool> AddUserProductMappingContact(long productId,
            long userId,
            string name,
            string phoneNumber,
            string companyEmail,
            string companyName);

        Task<bool> DeleteProductMappingsByUserId(long userId);

        Task<List<long>> GetProductIdsByUserIdAsync(long userId);
    }
}
