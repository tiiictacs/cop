﻿using CustomerPortal.Infrastructure.Services;
using CustomerPortal.Services.Models.Identity;

namespace CustomerPortal.Services.Services.Interfaces
{
    public interface ISingpassService:IService
    {
        string GetAuthoriseUrl();

        MyInfoDto GetMyInfo(string code);
    }
}
