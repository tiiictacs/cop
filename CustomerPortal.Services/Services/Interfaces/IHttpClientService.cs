﻿using CustomerPortal.Infrastructure.Services;
using CustomerPortal.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerPortal.Services.Services.Interfaces
{
    public interface IHttpClientService:IService
    {
        GenericResult PostSingpass(string url, object parameters, string authHeader);

        GenericResult GetSingpass(string url, string authHeader);

    }
}
