﻿using CustomerPortal.Infrastructure.Services;
using CustomerPortal.Services.Models.User;
using System.Threading.Tasks;

namespace CustomerPortal.Services.Services.Interfaces
{
    public interface IUserContactService : IService
    {
        Task<UserContactDto> AddUserContactAsync(UserContactDto dto);

        Task<bool> DeleteUserContactAsync(long id);

        Task<UserContactDto> GetByUenOrBrnAsync(string searchQuery);

        Task<UserContactDto> GetByUserIdAsync(long id);
    }
}
