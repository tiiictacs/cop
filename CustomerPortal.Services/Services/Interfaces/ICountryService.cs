﻿using CustomerPortal.Infrastructure.Services;
using CustomerPortal.Services.Blog.Dto;
using CustomerPortal.Services.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerPortal.Services.Services.Interfaces
{
    public interface ICountryService : IService
    {
        Task<List<CountryDto>> GetCountries(); 
    }
}
