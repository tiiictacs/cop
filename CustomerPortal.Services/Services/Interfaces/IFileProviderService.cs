﻿using CustomerPortal.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerPortal.Services.Services.Interfaces
{
    public interface IFileProviderService:IService
    {

        string GetAbsolutePath(params string[] paths);
    }
}
