﻿using CustomerPortal.Infrastructure.Services;
using CustomerPortal.Services.Models.Identity;

namespace CustomerPortal.Services.Services.Interfaces
{
    public interface ICorppassService:IService
    {
        string GetAuthoriseUrl();

        MyInfoDto GetMyInfo(string code);
    }
}
