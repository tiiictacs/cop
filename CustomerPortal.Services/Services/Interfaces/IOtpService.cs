﻿using CustomerPortal.Infrastructure.Services;
using CustomerPortal.Services.Models;
using System.Threading.Tasks;

namespace CustomerPortal.Services.Services.Interfaces
{
    public interface IOtpService:IService
    {
        Task<GenericResult> GenerateEmailVerificationOtp(string email);


        Task<GenericResult> VerifyOtp(string email, string otp);

        Task<GenericResult> VerifyProductOtpAsync(string email, string otp, string entityID);

        Task<GenericResult> GenerateProductVerificationOtp(string email, string entityID);
    }
}
