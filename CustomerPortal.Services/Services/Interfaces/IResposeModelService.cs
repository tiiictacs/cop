﻿using CustomerPortal.Infrastructure.Services;
using CustomerPortal.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerPortal.Services.Services.Interfaces
{
    public interface IResposeModelService : IService
    {
        ResponseModel GetRespons(int status, string message, object data = null, List<string> validationMessage = null, string errorMessage = null);
    }
}
