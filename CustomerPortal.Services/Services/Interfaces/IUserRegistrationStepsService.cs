﻿using CustomerPortal.Infrastructure.Services;
using CustomerPortal.Services.Models.User;
using System.Threading.Tasks;

namespace CustomerPortal.Services.Services.Interfaces
{
    public interface IUserRegistrationStepsService : IService
    {
        Task<UserRegistrationStepDto> AddUserRegistrationStepAsync(UserRegistrationStepDto dto);

        Task<bool> DeleteUserRegistrationStepAsync(long id);

        Task<UserRegistrationStepDto> GetByGuestIdAndStepAsync(string guestId, int step);

        Task<bool> DeleteUserRegistrationStepAsync(string guestId);
    }
}
