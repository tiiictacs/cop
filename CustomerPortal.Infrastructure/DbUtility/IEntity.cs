namespace CustomerPortal.Infrastructure.DbUtility
{
    public interface IEntity
    {
        long Id { get; set; }
    }
}
