﻿namespace CustomerPortal.Infrastructure.Settings
{
    public interface ILoggerConfiguration
    {
        string LogsFolder { get; }
    }
}
