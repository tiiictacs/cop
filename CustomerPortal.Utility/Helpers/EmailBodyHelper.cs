﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerPortal.Utility.Helpers
{
    public static class EmailBodyHelper
    {
        public static string BuildOtpVerificationEmail(string emailAddress,string otpCode)
        {
            var bodyTemplate = @"<p>Dear CUSTOMER_EMAIL ,<br />
                                <br />
                                Please enter the OTP verification code to continue the registering account process.<br />
                                <br />
                                OTP_CODE  <br />
                                <br />
                                If you did not create an account, no further action is required.<br />
                                <br />
                                If you have any questions, you can reply to this email to contact support.<br />
                                <br />
                                <br />
                                Best Regards,<br />
                                SingPost Admin.</p>
                                ";

            var body = bodyTemplate.Replace("CUSTOMER_EMAIL", emailAddress);
            body = body.Replace("OTP_CODE", otpCode);

            return body;    
        }

        public static string BuildOtpVerificationProduct(string fullName, List<string> verificationUrl)
        {
            var bodyTemplate = @"<p>Dear CUSTOMER_FULL_NAME ,<br />
                                <br />
                                We received your account's application. Please access the link(s) below to activate the selected product(s).<br />
                                <br />
                                The activation link will expire in 24 hours. <br />
                                <br />

                                PRODUCT_LINK

                                <br />
                                <br />
                                Best Regards,<br />
                                SingPost Admin.</p>
                                ";

            var sb = new StringBuilder();
            foreach (var verification in verificationUrl)
            {
                sb.Append(verification);
                sb.Append("<br />");
            }

            var body = bodyTemplate.Replace("CUSTOMER_FULL_NAME", fullName);
            body = body.Replace("PRODUCT_LINK", sb.ToString());

            return body;
        }
    }
}
