﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerPortal.Utility.Helpers
{
    public static class TextHelper
    {
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        const string numberChars = "0123456789";

        public static string RandomString(int length)
        {
            Random random = new Random();
            
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string RandomNumber(int length)
        {
            Random random = new Random();

            return new string(Enumerable.Repeat(numberChars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
