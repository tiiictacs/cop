﻿namespace CustomerPortal.Api.Models
{
    public class ProductContactApiDto
    {
        public string Email {  get; set; }

        public long ProductId {  get; set; }

        public string Name { get; set; }

        public string PhoneNumber {  get; set; }    

        public string CompanyEmail { get; set; }   

        public string CompanyName {  get; set; }    
    }
}
