﻿namespace CustomerPortal.Api.Models
{
    public class UserRegistrationStepApiDto
    { 
        public string JsonData {  get; set; }   

        public int Step {  get; set; }  

        public string GuestId {  get; set; }    
    }
}
