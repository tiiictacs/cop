﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CustomerPortal.Api.Models
{
    public class RegisterUserApiDto
    {
        [Required]
        public string Email {  get; set; } 
        [Required]
        public string FullName { get; set; } 
        [Required]
        public string CountryCode { get; set; }
         
        public bool IsBillingSameRegistration {  get; set; }
        [Required]
        public string PhoneNumber {  get; set; }

        public BussinessApiDto Bussiness { get; set; } = new BussinessApiDto();

        public BussinessApiDto Billing { get; set; } = new BussinessApiDto();

        public List<long> ProductIds { get; set; }  

    }
}
