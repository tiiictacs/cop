﻿namespace CustomerPortal.Api.Models
{
    public class BussinessApiDto
    {
        public string UEN { get; set; }

        public string BRN { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string UnitNo { get; set; }

        public string PostalCode { get; set; } 
    }
}
