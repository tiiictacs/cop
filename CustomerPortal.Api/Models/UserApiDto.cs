﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CustomerPortal.Api.Models
{
    public class UserApiDto
    { 
        public string Email { get; set; }
         
        public string FullName { get; set; }
         
        public string CountryCode { get; set; }

        public string PhoneNumber { get; set; }

        public BussinessApiDto Bussiness { get; set; } = new BussinessApiDto();

        public BussinessApiDto Billing { get; set; } = new BussinessApiDto();

        public bool IsBillingSameRegistration { get; set; }

        public List<long> ProductIds { get; set; }
    }
}
