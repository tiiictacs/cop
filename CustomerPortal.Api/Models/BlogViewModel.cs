﻿using System.ComponentModel.DataAnnotations;

namespace CustomerPortal.Api.Models
{
    public class BlogViewModel
    {
        [Required]
        public long Id { get; set; }
        [Required]
        public string Url { get; set; }
    }
}
