﻿namespace CustomerPortal.Api.Models
{
    public class ProductApiDto
    {
        public long Id { get; set; }    

        public string Name { get; set; }
    }
}
