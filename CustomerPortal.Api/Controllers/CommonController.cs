﻿using Microsoft.AspNetCore.Mvc;
using CustomerPortal.Services.Models;
using CustomerPortal.Services.Services.Interfaces;
using System.Threading.Tasks;

namespace CustomerPortal.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommonController : Controller
    {
        private readonly ICountryService _countryService;

        private readonly IResposeModelService _resposeModelService;

        private readonly IProductService _productService;

        public CommonController(
            ICountryService countryService,
            IResposeModelService resposeModelService,
            IProductService productService)
        {
            _countryService = countryService;
            _resposeModelService = resposeModelService;
            _productService = productService;
        }

        [HttpGet, Route("GetCountries")]
        public async Task<IActionResult> GetCountries()
        {
            var countries = await _countryService.GetCountries();

            if (countries == null)
                return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "ERROR"));

            return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Success, "", countries));
        }

        [HttpGet, Route("GetProducts")]
        public async Task<IActionResult> GetProducts()
        {
            var products = await _productService.GetProducts();

            if (products == null)
                return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "ERROR"));

            return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Success, "", products));
        }
    }
}
