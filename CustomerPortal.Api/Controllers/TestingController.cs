﻿using Microsoft.AspNetCore.Mvc;
using CustomerPortal.DbContext.Enums;
using CustomerPortal.Services.Models;
using CustomerPortal.Services.Services.Interfaces;
using System.Threading.Tasks;

namespace CustomerPortal.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestingController : Controller
    {
        private readonly ICountryService _countryService;
        private readonly IEmailTemplateService _emailTemplateService;
        private readonly IResposeModelService _resposeModelService;

        public TestingController(
            ICountryService countryService,
            IEmailTemplateService emailTemplateService,
            IResposeModelService resposeModelService)
        {
            _countryService = countryService;
            _emailTemplateService = emailTemplateService;
            _resposeModelService = resposeModelService;
        }

        [HttpGet, Route("GetCountries")]
        public async Task<IActionResult> GetCountries()
        {
            var countries = await _countryService.GetCountries();

            if (countries == null)
                return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "ERROR"));

            return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Success, "", countries));
        }

        [HttpGet, Route("GetEmailTemplate")]
        public async Task<IActionResult> GetEmailTemplate()
        {
            var item = await _emailTemplateService.GetEmailTemplateByType(EmailTemplateType.WaitingForApproval);

            if (item == null)
                return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "ERROR"));

            return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Success, "", item));
        }
    }
}
