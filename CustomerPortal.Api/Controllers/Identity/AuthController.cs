﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using CustomerPortal.Api.Models;
using CustomerPortal.DbContext.Entities;
using CustomerPortal.DbContext.Entities.Identity;
using CustomerPortal.Infrastructure.Settings;
using CustomerPortal.Services.Email;
using CustomerPortal.Services.Models;
using CustomerPortal.Services.Models.User;
using CustomerPortal.Services.Services.Interfaces;
using CustomerPortal.Utility.Helpers;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CustomerPortal.Api.Controllers.Identity
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly IConfiguration _configuration;
        private readonly IEmailService _emailService;
        private readonly IJwtSettings _jwt;
        private readonly IClientAppSettings _clientAppSettings;
        private readonly ISingpassService _singpassService;
        private readonly ICorppassService _corppassService;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IResposeModelService _resposeModelService; 
        private readonly IOtpService _otpService; 
        private readonly IUserContactService _userContactService;
        private readonly IProductService _productService;
        private readonly IUserRegistrationStepsService _userRegistrationStepsService;

        public AuthController(
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
            IConfiguration configuration,
            IEmailService emailService,
            IJwtSettings jwt,
            SignInManager<ApplicationUser> signInManager,
            IClientAppSettings clientAppSettings,
            ISingpassService singpassService,
            IResposeModelService resposeModelService,
            ICorppassService corppassService,
            IOtpService otpService,
            IUserContactService userContactService,
            IProductService productService,
            IUserRegistrationStepsService userRegistrationStepsService)
        {
            _userContactService = userContactService;
            _otpService = otpService;
            _userManager = userManager;
            _roleManager = roleManager;
            _configuration = configuration;
            _emailService = emailService;
            _jwt = jwt;
            _signInManager = signInManager;
            _clientAppSettings = clientAppSettings;
            _singpassService = singpassService;
            _resposeModelService = resposeModelService;
            _corppassService = corppassService;
            _productService = productService;
            _userRegistrationStepsService = userRegistrationStepsService;
        }

        [HttpPost("ConfirmEmail")]
        public async Task<IActionResult> ConfirmEmail([FromBody] ConfirmEmailViewModel model)
        {
            if (model.UserId == null || model.Code == null)
            {
                return BadRequest(new string[] { "Error retrieving information!" });
            }

            ApplicationUser user = await _userManager.FindByIdAsync(model.UserId).ConfigureAwait(false);
            if (user == null)
                return BadRequest(new string[] { "Could not find user!" });

            IdentityResult result = await _userManager.ConfirmEmailAsync(user, model.Code).ConfigureAwait(false);
            if (!result.Succeeded)
                return BadRequest(new string[] { "Something went wront confirming user email." });

            var lockoutEnabledResult = await _userManager.SetLockoutEnabledAsync(user, false);
            if (!lockoutEnabledResult.Succeeded)
            {
                return BadRequest(new string[] { "Something went wront disabling user lockout." });
            }

            return Ok();
            // return BadRequest(result.Errors.Select(x => x.Description));
        }
         
        [HttpPost("Login")]
        public async Task<IActionResult> Login([FromBody] LoginViewModel model)
        {
            ApplicationUser user = await _userManager.FindByEmailAsync(model.Email).ConfigureAwait(false);
            if (user == null)
                return BadRequest(new string[] { "Invalid credentials." });

            TokenModel tokenModel = new TokenModel()
            {
                HasVerifiedEmail = false
            };

            // Only allow login if email is confirmed
            if (!user.EmailConfirmed)
            {
                return Ok(tokenModel);
            }

            // Used as user lock
            if (user.LockoutEnabled)
                return BadRequest(new string[] { "This account has been locked." });

            if (await _userManager.CheckPasswordAsync(user, model.Password).ConfigureAwait(false))
            {
                tokenModel.HasVerifiedEmail = true;

                if (user.TwoFactorEnabled)
                {
                    tokenModel.TFAEnabled = true;
                    return Ok(tokenModel);
                }
                else
                {
                    JwtSecurityToken jwtSecurityToken = await CreateJwtToken(user, model.RememberMe).ConfigureAwait(false);
                    tokenModel.TFAEnabled = false;
                    tokenModel.Token = new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);

                    return Ok(tokenModel);
                }
            }

            return BadRequest(new string[] { "Invalid login attempt." });
        }

        [HttpGet("Logout")]
        public async Task<IActionResult> Logout()
        {
            return Ok(); // LOL
        }

        [HttpPost("LoginWith2fa")]
        public async Task<IActionResult> LoginWith2fa([FromBody] LoginWith2faViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.Values.Select(x => x.Errors.FirstOrDefault().ErrorMessage));

            ApplicationUser user = await _userManager.FindByEmailAsync(model.Email).ConfigureAwait(false);
            if (user == null)
                return BadRequest(new string[] { "Invalid credentials." });

            if (await _userManager.VerifyTwoFactorTokenAsync(user, "Authenticator", model.TwoFactorCode).ConfigureAwait(false))
            {
                JwtSecurityToken jwtSecurityToken = await CreateJwtToken(user, model.RememberMe).ConfigureAwait(false);

                TokenModel tokenModel = new TokenModel()
                {
                    HasVerifiedEmail = true,
                    TFAEnabled = false,
                    Token = new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken)
                };

                return Ok(tokenModel);

            }
            return BadRequest(new string[] { "Unable to verify Authenticator Code!" });
        }

        [HttpPost("ForgotPassword")]
        public async Task<IActionResult> ForgotPassword([FromBody] ForgotPasswordViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.Values.Select(x => x.Errors.FirstOrDefault().ErrorMessage));

            ApplicationUser user = await _userManager.FindByEmailAsync(model.Email).ConfigureAwait(false);
            if (user == null || !(await _userManager.IsEmailConfirmedAsync(user).ConfigureAwait(false)))
                return BadRequest(new string[] { "Please verify your email address." });

            string code = await _userManager.GeneratePasswordResetTokenAsync(user).ConfigureAwait(false);

            var callbackUrl = $"{_clientAppSettings.ClientBaseUrl}{_clientAppSettings.ResetPasswordPath}?uid={user.Id}&code={System.Net.WebUtility.UrlEncode(code)}";
            await _emailService.SendPasswordResetAsync(model.Email, callbackUrl).ConfigureAwait(false);

            return Ok();
        }

        [HttpPost("ResetPassword")]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.Values.Select(x => x.Errors.FirstOrDefault().ErrorMessage));

            ApplicationUser user = await _userManager.FindByIdAsync(model.UserId).ConfigureAwait(false);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return BadRequest(new string[] { "Invalid credentials." });
            }
            IdentityResult result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password).ConfigureAwait(false);
            if (result.Succeeded)
            {
                return Ok(result);
            }
            return BadRequest(result.Errors.Select(x => x.Description));
        }

        [HttpPost("ResendVerificationEmail")]
        public async Task<IActionResult> ResendVerificationEmail([FromBody] UserViewModel model)
        {
            ApplicationUser user = await _userManager.FindByEmailAsync(model.Email).ConfigureAwait(false);
            if (user == null)
                return BadRequest(new string[] { "Could not find user!" });

            string code = await _userManager.GenerateEmailConfirmationTokenAsync(user).ConfigureAwait(false);

            var callbackUrl = $"{_clientAppSettings.ClientBaseUrl}{_clientAppSettings.EmailConfirmationPath}?uid={user.Id}&code={System.Net.WebUtility.UrlEncode(code)}";
            await _emailService.SendEmailConfirmationAsync(user.Email, callbackUrl).ConfigureAwait(false);

            return Ok();
        }

        private async Task<JwtSecurityToken> CreateJwtToken(ApplicationUser user, bool rememberMe)
        {
            IList<Claim> userClaims = await _userManager.GetClaimsAsync(user).ConfigureAwait(false);
            IList<string> roles = await _userManager.GetRolesAsync(user).ConfigureAwait(false);

            var roleClaims = new List<Claim>();

            for (int i = 0; i < roles.Count; i++)
            {
                roleClaims.Add(new Claim("roles", roles[i]));
            }

            string ipAddress = IpHelper.GetIpAddress();

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim("uid", user.Id.ToString()),
                // should be like this if we have author 1:1 relationship with User table
                // new Claim("aid", user.Author.ApplicationUserId.ToString()), 
                // but because of the VueBoilerplate, we have to check stuff 
                new Claim("ip", ipAddress)
            }
            .Union(userClaims)
            .Union(roleClaims);

            SymmetricSecurityKey symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwt.Key));
            SigningCredentials signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256);

            JwtSecurityToken jwtSecurityToken = new JwtSecurityToken(
            issuer: _jwt.Issuer,
            audience: _jwt.Audience,
            claims: claims,
            expires: DateTime.UtcNow.AddMinutes(_jwt.DurationInMinutes + (rememberMe ? _jwt.RememberMeDurationInHours * 60 : 0)),
            signingCredentials: signingCredentials
            );
            return jwtSecurityToken;
        }

        [AllowAnonymous]
        [HttpGet("SingpassAuthoriseUrl")]
        public IActionResult GetSingpassAuthoriseUrl()
        {
            var authoriseUrl = _singpassService.GetAuthoriseUrl();

            return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Success, "", authoriseUrl));
        }

        [HttpPost("SingpassCallback")]
        public IActionResult PostSingpassCallback(string code)
        {
            var myinfo = _singpassService.GetMyInfo(code);

            if (myinfo!=null)
            {
                return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Success, "", myinfo));

            }
            return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "ERROR"));
        }

        [AllowAnonymous]
        [HttpGet("CorpassAuthoriseUrl")]
        public IActionResult GetCorpassAuthoriseUrl()
        {
            var authoriseUrl = _corppassService.GetAuthoriseUrl();

            return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Success, "", authoriseUrl));
        }

        [HttpPost("CorpassCallback")]
        public IActionResult PostCorpassCallback(string code)
        {
            var myinfo = _corppassService.GetMyInfo(code);

            if (myinfo != null)
            {
                return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Success, "", myinfo));

            }
            return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "ERROR"));
        }

        [AllowAnonymous]
        [HttpPost("GenerateOtp/{emailaddress}")]
        public async Task<IActionResult> GenerateOtp(string emailaddress)
        {
            try
            {
                if (string.IsNullOrEmpty(emailaddress))
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Email address not found"));

                var otpResult = await _otpService.GenerateEmailVerificationOtp(emailaddress);

                if (!otpResult.Success)
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Failed to generate token"));

                var body = EmailBodyHelper.BuildOtpVerificationEmail(emailaddress, otpResult.Message);

                await _emailService.SendAsync("Verify your email address", body, emailaddress);

                return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Success, ""));
            }
            catch (Exception e)
            {

            }

            return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Failed to generate token"));

        }

        [AllowAnonymous]
        [HttpPost("VerifyOtp")]
        public async Task<IActionResult> VerifyOtp(string emailaddress, string otp)
        {
            try
            {
                if (string.IsNullOrEmpty(emailaddress) || string.IsNullOrEmpty(otp))
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Invalid parameters"));

                var otpResult = await _otpService.VerifyOtp(emailaddress, otp);

                if (!otpResult.Success)
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Invalid otp"));

                return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Success, ""));
            }
            catch (Exception e)
            {


            }

            return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Failed to verify otp"));

        }


        [AllowAnonymous]
        [HttpPost("Register")]
        public async Task<IActionResult> Register(RegisterUserApiDto apiRequest)
        {
            try
            {
                if (string.IsNullOrEmpty(apiRequest.Email))
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Invalid parameters"));

                if (apiRequest.ProductIds.Count == 0 )
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Invalid parameters"));

                if (!apiRequest.IsBillingSameRegistration && string.IsNullOrEmpty(apiRequest.Billing.Address))
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Invalid parameters"));

                var products = await _productService.GetProducts();

                if (products.Count == 0)
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Register failed"));

                foreach (var productId in apiRequest.ProductIds)
                {
                    if (!products.Any(d=>d.Id == productId))
                        return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Product not found"));
                }

                var user = await _userManager.FindByEmailAsync(apiRequest.Email);

                if (user != null)
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "User existing"));

                var newUser = new ApplicationUser
                {
                    Email = apiRequest.Email,
                    FullName = apiRequest.FullName,
                    PhoneNumber = apiRequest.PhoneNumber,
                    UserName = apiRequest.Email
                };

                var result = await _userManager.CreateAsync(newUser);

                if (!result.Succeeded)
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Register failed"));

                var userContact  = await _userContactService.AddUserContactAsync(new Services.Models.User.UserContactDto
                {
                    Address = apiRequest.Bussiness.Address,
                    BillingAddress = apiRequest.IsBillingSameRegistration ? apiRequest.Bussiness.Address : apiRequest.Billing.Address,
                    BillingUnitNo = apiRequest.IsBillingSameRegistration ? apiRequest.Bussiness.UnitNo : apiRequest.Billing.UnitNo,
                    BillingName = apiRequest.IsBillingSameRegistration ? apiRequest.Bussiness.Name : apiRequest.Bussiness.Name,
                    BillingPostalCode = apiRequest.IsBillingSameRegistration ? apiRequest.Bussiness.PostalCode : apiRequest.Billing.PostalCode,
                    BRN = apiRequest.Bussiness.BRN, 
                    CountryCode = apiRequest.CountryCode,
                    Name = apiRequest.Bussiness.Name, 
                    UEN = apiRequest.Bussiness.UEN,
                    UnitNo = apiRequest.Bussiness.UnitNo,
                    UserId = newUser.Id
                });

                if (userContact == null)
                {
                    await _userManager.DeleteAsync(newUser);
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Register failed"));
                }

                var addingProductResult = await _productService.AddProductMappings(apiRequest.ProductIds, newUser.Id);

                if (!addingProductResult)
                {
                    await _userManager.DeleteAsync(newUser);
                    await _userContactService.DeleteUserContactAsync(userContact.Id);
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Register failed"));
                }

                var ret = await SendProductVerificationEmail(newUser, apiRequest.ProductIds);

                if (!ret)
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Sending verification product failed"));

                return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Success, ""));
            }
            catch (Exception e)
            { 

            }

            return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Register failed"));

        }

        [AllowAnonymous]
        [HttpPost("UpdateInformation")]
        public async Task<IActionResult> UpdateInformation(RegisterUserApiDto apiRequest)
        {
            try
            {
                if (string.IsNullOrEmpty(apiRequest.Email))
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Invalid parameters"));

                if (apiRequest.ProductIds.Count == 0)
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Invalid parameters"));

                if (!apiRequest.IsBillingSameRegistration && string.IsNullOrEmpty(apiRequest.Billing.Address))
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Invalid parameters"));

                var products = await _productService.GetProducts();

                if (products.Count == 0)
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Register failed"));

                foreach (var productId in apiRequest.ProductIds)
                {
                    if (!products.Any(d => d.Id == productId))
                        return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Product not found"));
                }

                var user = await _userManager.FindByEmailAsync(apiRequest.Email);

                if (user == null)
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "User not existing"));


                user.FullName = apiRequest.FullName;
                user.PhoneNumber = apiRequest.PhoneNumber;

                var result = await _userManager.UpdateAsync(user); 

                if (!result.Succeeded)
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Update failed"));

                await _userContactService.DeleteUserContactAsync(user.Id);

                var userContact = await _userContactService.AddUserContactAsync(new Services.Models.User.UserContactDto
                {
                    Address = apiRequest.Bussiness.Address,
                    BillingAddress = apiRequest.IsBillingSameRegistration ? apiRequest.Bussiness.Address : apiRequest.Billing.Address,
                    BillingUnitNo = apiRequest.IsBillingSameRegistration ? apiRequest.Bussiness.UnitNo : apiRequest.Billing.UnitNo,
                    BillingName = apiRequest.IsBillingSameRegistration ? apiRequest.Bussiness.Name : apiRequest.Bussiness.Name,
                    BillingPostalCode = apiRequest.IsBillingSameRegistration ? apiRequest.Bussiness.PostalCode : apiRequest.Billing.PostalCode,
                    BRN = apiRequest.Bussiness.BRN,
                    CountryCode = apiRequest.CountryCode,
                    Name = apiRequest.Bussiness.Name,
                    UEN = apiRequest.Bussiness.UEN,
                    UnitNo = apiRequest.Bussiness.UnitNo,
                    UserId = user.Id
                });

                if (userContact == null)
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Update failed"));

                await _productService.DeleteProductMappingsByUserId(user.Id);

                var addingProductResult = await _productService.AddProductMappings(apiRequest.ProductIds, user.Id);

                if (!addingProductResult)
                { 
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Update failed"));
                }

                var ret = await SendProductVerificationEmail(user, apiRequest.ProductIds);

                if (!ret)
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Sending verification product failed"));

                return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Success, ""));
            }
            catch (Exception e)
            {

            }

            return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Register failed"));

        }

        async Task<bool> SendProductVerificationEmail(ApplicationUser user, List<long> productIds)
        {

            var baseUrl = _configuration["AppSettings:ClientBaseUrl"];

            if (string.IsNullOrEmpty(baseUrl)) return false;

            var links = new List<string>(); 

            foreach (var productId in productIds)
            {
                var product = await _productService.GetByIdAsync(productId);

                if (product == null) continue;

                var otpResult = await _otpService.GenerateProductVerificationOtp(user.Email, productId.ToString());

                if (otpResult == null) { continue; }

                var verificationLink = $"{baseUrl}/auth/subscribe-form?email={user.Email}&productId={productId}&code={otpResult.Message}";

                links.Add($"<a href='{verificationLink}'>{verificationLink}<a>");

            }

            if (links.Count == 0) return false;

            var body = EmailBodyHelper.BuildOtpVerificationProduct(user.FullName, links);

            await _emailService.SendAsync("Fill out your Product form", body, user.Email);

            return true;
        }

        [AllowAnonymous]
        [HttpPost("VerifyProductOtp")]
        public async Task<IActionResult> VerifyProductOtp(string emailaddress, string otp, long productId)
        {
            try
            {
                if (string.IsNullOrEmpty(emailaddress) || string.IsNullOrEmpty(otp) || productId == 0)
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Invalid parameters"));

                var otpResult = await _otpService.VerifyProductOtpAsync(emailaddress, otp, productId.ToString());

                if (!otpResult.Success)
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Invalid otp"));

                var userInfo = await _userManager.FindByEmailAsync(emailaddress);

                var result = await _productService.VerificationUserProduct(productId, userInfo.Id);

                if (result)
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Success, ""));


            }
            catch (Exception e)
            {


            }

            return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Failed to verify otp"));

        }

        [AllowAnonymous]
        [HttpPost("AddProductContact")]
        public async Task<IActionResult> AddProductContact(ProductContactApiDto request)
        {
            try
            {
                if (string.IsNullOrEmpty(request.Email) 
                    || request.ProductId == 0 )
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Invalid parameters"));
                 
                var userInfo = await _userManager.FindByEmailAsync(request.Email);

                if (userInfo == null)
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "User not found"));
                 

                var result = await _productService.AddUserProductMappingContact(request.ProductId,
                                        userInfo.Id,
                                        request.Name,
                                        request.PhoneNumber,
                                        request.CompanyEmail,
                                        request.CompanyName );

                if (result)
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Success, ""));


            }
            catch (Exception e)
            { 
            }

            return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Failed to verify otp"));

        }


        [AllowAnonymous]
        [HttpPost("GetUserRegistrationStep")]
        public async Task<IActionResult> GetUserRegistrationStep(UserRegistrationStepApiDto request)
        {
            try
            {
                if (string.IsNullOrEmpty(request.GuestId)
                    || request.Step == 0)
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Invalid parameters"));

                var userRegistrationStep = await _userRegistrationStepsService.GetByGuestIdAndStepAsync(request.GuestId, request.Step);

                if (userRegistrationStep == null)
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Not found"));

                var userRegistrationStepDto = new UserRegistrationStepApiDto
                {
                     JsonData = userRegistrationStep.JsonData
                };

                return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Success, "", userRegistrationStepDto));
            }
            catch (Exception e)
            {
            }

            return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Not found"));

        }


        [AllowAnonymous]
        [HttpPost("AddUserRegistrationStep")]
        public async Task<IActionResult> AddUserRegistrationStep(UserRegistrationStepApiDto request)
        {
            try
            {
                if (string.IsNullOrEmpty(request.GuestId)
                    || request.Step == 0)
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Invalid parameters"));

                var userRegistrationStep = await _userRegistrationStepsService.GetByGuestIdAndStepAsync(request.GuestId, request.Step);

                if (userRegistrationStep == null)
                {
                    var userRegistrationStepDto = await _userRegistrationStepsService.AddUserRegistrationStepAsync(new UserRegistrationStepDto
                    {
                        JsonData = request.JsonData,
                        GuestId = request.GuestId,
                        Step = request.Step,
                    });

                    if (userRegistrationStepDto.Id == 0)
                        return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Add data failed"));

                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Success, "", userRegistrationStepDto));
                }
                 
                var ret = await _userRegistrationStepsService.DeleteUserRegistrationStepAsync(userRegistrationStep.Id);

                if (!ret)
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Add data failed"));

                var userRegistrationStepDtoResult = await _userRegistrationStepsService.AddUserRegistrationStepAsync(new UserRegistrationStepDto
                {
                    JsonData = request.JsonData,
                    GuestId = request.GuestId,
                    Step = request.Step,
                });


                return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Success, "", userRegistrationStepDtoResult));
            }
            catch (Exception e)
            {
            }

            return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Error"));

        }


        [AllowAnonymous]
        [HttpPost("DeleteUserRegistrationStep")]
        public async Task<IActionResult> DeleteUserRegistrationStep(UserRegistrationStepApiDto request)
        {
            try
            {
                if (string.IsNullOrEmpty(request.GuestId))
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Invalid parameters"));

                  
                var ret = await _userRegistrationStepsService.DeleteUserRegistrationStepAsync(request.GuestId);

                if (!ret)
                    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Delete data failed"));

                 
                return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Success, ""));
            }
            catch (Exception e)
            {
            }

            return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Error"));

        }
    }
}
