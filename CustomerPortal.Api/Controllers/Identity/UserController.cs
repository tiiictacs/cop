﻿using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using CustomerPortal.Api.Models;
using CustomerPortal.DbContext.Entities.Identity;
using CustomerPortal.Services.Email;
using CustomerPortal.Services.Models;
using CustomerPortal.Services.Services.Interfaces;

namespace CustomerPortal.Api.Controllers.Identity
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly IResposeModelService _resposeModelService;
        private readonly IOtpService _otpService;
        private readonly IEmailService _emailService;
        private readonly IUserContactService _userContactService; 
        private readonly IProductService _productService;

        public UserController(
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
            IResposeModelService resposeModelService,
            IOtpService otpService,
            IEmailService emailService,
            IUserContactService userContactService,
            IProductService productService
            )
        {
            this._userManager = userManager;
            this._roleManager = roleManager;
            _resposeModelService = resposeModelService;
            _otpService = otpService;
            _emailService = emailService;   
            _userManager = userManager;
            _userContactService = userContactService;
            _productService = productService;
        }

        //[HttpGet]
        //[ProducesResponseType(typeof(IEnumerable<IdentityUser>), 200)]
        //[Route("Get")]
        //public IActionResult Get() {
        //    var infos = _userManager.Users.Select(user => new
        //    {
        //        user.Id,
        //        user.Email,
        //        user.PhoneNumber,
        //        user.EmailConfirmed,
        //        user.LockoutEnabled,
        //        user.TwoFactorEnabled
        //    });

        //    return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Success, "", infos));
        //}

        //[HttpGet("Get/{Id}")]
        //public IActionResult Get(int id)
        //{
        //    if (string.IsNullOrEmpty(id.ToString()))
        //        return BadRequest(new string[] { "Empty parameter!" });

        //    return Ok(_userManager.Users
        //        .Where(user => user.Id == id)
        //        .Select(user => new
        //        {
        //            user.Id,
        //            user.Email,
        //            user.PhoneNumber,
        //            user.EmailConfirmed,
        //            user.LockoutEnabled,
        //            user.TwoFactorEnabled
        //        })
        //        .FirstOrDefault());
        //}

        [AllowAnonymous]
        [HttpGet("Get/{emailaddress}")]
        public async Task<IActionResult> Get(string emailaddress)
        {
            if (string.IsNullOrEmpty(emailaddress))
                return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Email address not found"));

            var info = await _userManager.FindByEmailAsync(emailaddress); 
             
            if (info==null)
                return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Email address not found"));

            

            var userContact = await _userContactService.GetByUserIdAsync(info.Id);

            if (userContact == null)
                return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "User contact not found"));


            var selectedProducts = await _productService.GetProductIdsByUserIdAsync(info.Id);

            var userInfo = new UserApiDto
            {
                FullName = info.FullName,
                Email = info.Email,
                CountryCode = userContact.CountryCode,
                PhoneNumber = info.PhoneNumber,
                IsBillingSameRegistration = userContact.Address == userContact.BillingAddress,
                ProductIds = selectedProducts,

                Bussiness = new BussinessApiDto
                {
                    Address = userContact.Address,
                    Name = userContact.Name,
                    PostalCode = userContact.PostalCode,    
                    UnitNo = userContact.UnitNo, 
                },
                Billing = new BussinessApiDto { 
                    Address = userContact.BillingAddress,
                    Name = userContact.BillingName,
                    PostalCode = userContact.BillingPostalCode,
                    UnitNo = userContact.BillingUnitNo
                
                }
            };

            return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Success, "", userInfo)); 
        }

        [AllowAnonymous]
        [HttpGet("GetRetriveBussinessInfo")]
        public async Task<IActionResult> GetRetriveBussinessInfo(string query)
        {
            if (string.IsNullOrEmpty(query))
                return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Invalid parameters"));

            var business = await _userContactService.GetByUenOrBrnAsync(query);

            if (business == null)
                return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Failed, "Bussiness not found"));

            return Ok(_resposeModelService.GetRespons(ResponseStatusCodes.Success, "", new BussinessApiDto
            {
                Address = business.Address,
                BRN = business.BRN, 
                Name = business.Name,
                PostalCode = business.PostalCode, 
                UEN = business.UEN,
                UnitNo = business.UnitNo
                
            }));

        }


        //[HttpPost("InsertWithRole")]
        //public async Task<IActionResult> InsertWithRole([FromBody] UserViewModel model)
        //{
        //    if (!ModelState.IsValid)
        //        return BadRequest(ModelState.Values.Select(x => x.Errors.FirstOrDefault().ErrorMessage));

        //    ApplicationUser user = new ApplicationUser
        //    {
        //        UserName = model.Email,
        //        Email = model.Email,
        //        EmailConfirmed = model.EmailConfirmed,
        //        PhoneNumber = model.PhoneNumber
        //    };

        //    ApplicationRole role = await _roleManager.FindByIdAsync(model.RoleId).ConfigureAwait(false);
        //    if (role == null)
        //        return BadRequest(new string[] { "Could not find role!" });

        //    IdentityResult result = await _userManager.CreateAsync(user, model.Password).ConfigureAwait(false);
        //    if (result.Succeeded)
        //    {
        //        IdentityResult result2 = await _userManager.AddToRoleAsync(user, role.Name).ConfigureAwait(false);
        //        if (result2.Succeeded)
        //        {
        //            return Ok(new
        //            {
        //                user.Id,
        //                user.Email,
        //                user.PhoneNumber,
        //                user.EmailConfirmed,
        //                user.LockoutEnabled,
        //                user.TwoFactorEnabled
        //            });
        //        }
        //    }
        //    return BadRequest(result.Errors.Select(x => x.Description));
        //}

        //[HttpPut("Update/{Id}")]
        //public async Task<IActionResult> Put(int id, [FromBody] EditUserViewModel model)
        //{
        //    if (!ModelState.IsValid)
        //        return BadRequest(ModelState.Values.Select(x => x.Errors.FirstOrDefault().ErrorMessage));

        //    ApplicationUser user = await _userManager.FindByIdAsync(id.ToString()).ConfigureAwait(false);
        //    if (user == null)
        //        return BadRequest(new[] { "Could not find user!" });

        //    // Add more fields to update
        //    user.Email = model.Email;
        //    user.UserName = model.Email;
        //    user.EmailConfirmed = model.EmailConfirmed;
        //    user.PhoneNumber = model.PhoneNumber;
        //    user.LockoutEnabled = model.LockoutEnabled;
        //    user.TwoFactorEnabled = model.TwoFactorEnabled;

        //    IdentityResult result = await _userManager.UpdateAsync(user).ConfigureAwait(false);
        //    if (result.Succeeded)
        //    {
        //        return Ok();
        //    }
        //    return BadRequest(result.Errors.Select(x => x.Description));
        //}

        //[HttpDelete("Delete/{Id}")]
        //public async Task<IActionResult> Delete(int id)
        //{
        //    if (String.IsNullOrEmpty(id.ToString()))
        //        return BadRequest(new[] { "Empty parameter!" });

        //    ApplicationUser user = await _userManager.FindByIdAsync(id.ToString()).ConfigureAwait(false);
        //    if (user == null)
        //        return BadRequest(new[] { "Could not find user!" });

        //    IdentityResult result = await _userManager.DeleteAsync(user).ConfigureAwait(false);
        //    if (result.Succeeded)
        //    {
        //        return Ok();
        //    }
        //    return BadRequest(result.Errors.Select(x => x.Description));
        //}
    }
}
