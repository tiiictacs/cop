﻿using CustomerPortal.Infrastructure.DbUtility;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using CustomerPortal.DbContext.Enums;

namespace CustomerPortal.DbContext.Entities
{
    public class UserProductMapping:IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public long UserId { get; set; }   

        public long ProductId {  get; set; }    

        public DateTime CreatedOn { get; set; } 

        public ProductMappingStatusEnum Status { get; set; }    

        public DateTime? ActivatedOn {  get; set; } 

        public string Name { get; set; }    

        public string PhoneNumber { get; set; }

        public string CompanyName { get; set; }   

        public string CompanyEmail {  get; set; }   
    }
}
