﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;
using CustomerPortal.Infrastructure.DbUtility;

namespace CustomerPortal.DbContext.Entities.Identity
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser<long>, IEntity
    {
        [Required, Column(TypeName = "NVARCHAR(250)")]
        public string FullName { get; set; }
          
        [InverseProperty("UserNavigation")]
        public virtual ICollection<Logging> Loggings { get; set; }
         
    }
}
