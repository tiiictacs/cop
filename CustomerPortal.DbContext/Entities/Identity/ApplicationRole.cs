﻿using Microsoft.AspNetCore.Identity;
using CustomerPortal.Infrastructure.DbUtility;

namespace CustomerPortal.DbContext.Entities.Identity
{
    public class ApplicationRole : IdentityRole<long>, IEntity
    {
        public ApplicationRole() { }
        public ApplicationRole(string name) { Name = name; }
    }
}
