﻿using CustomerPortal.Infrastructure.DbUtility;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CustomerPortal.DbContext.Entities
{
    public class EmailTemplate : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public int Type { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public string To { get; set; }
    }
}
