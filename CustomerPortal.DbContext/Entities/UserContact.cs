﻿using CustomerPortal.Infrastructure.DbUtility;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Microsoft.VisualBasic;
using System;

namespace CustomerPortal.DbContext.Entities
{
    public class UserContact : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public string UEN { get; set; }

        public long UserId { get; set; }

        public string BRN { get; set; }

        public string CountryCode { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string UnitNo { get; set; }

        public string PostalCode { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string BillingName { get; set; }

        public string BillingAddress { get; set; }

        public string BillingUnitNo { get; set; }

        public string BillingPostalCode { get; set; }
          
        public DateTime CreatedOn {  get; set; }    



    }
}
