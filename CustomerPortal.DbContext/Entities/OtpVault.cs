﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using CustomerPortal.DbContext.Enums;
using CustomerPortal.Infrastructure.DbUtility;

namespace CustomerPortal.DbContext.Entities
{
    public class OtpVault : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public string Code { get; set; }

        public DateTime CreatedOn {  get; set; }    

        public string Email {  get; set; } 

        public DateTime ExpiredOn {  get; set; }

        public OtpVaultTypeEnum Type { get; set; }  

        public string EntityID {  get; set; }   
    }
}
