﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using CustomerPortal.Infrastructure.DbUtility;

namespace CustomerPortal.DbContext.Entities
{
    public class UserRegistrationStep : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public DateTime CreatedOn { get; set; }

        public string JsonData {  get; set; }   

        public int Step {  get; set; }  
          
        public DateTime UpdatedOn {  get; set; }    

        public string GuestId {  get; set; }
    }
}
