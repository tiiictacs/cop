﻿using CustomerPortal.Infrastructure.DbUtility;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CustomerPortal.DbContext.Entities
{
    public class Country : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public string Name { get; set; }    

        public string TwoLetterIsoCode {  get; set; }   

        public string ThreeLetterIsoCode { get; set; }  

        public int NumericIsoCode {  get; set; }    

        public byte Published {  get; set; } 

        public int DisplayOrder {  get; set; }  
    }
}
