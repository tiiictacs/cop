﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using CustomerPortal.DbContext.Entities;
using CustomerPortal.DbContext.Entities.Identity;
using CustomerPortal.DbContext.Interceptors;
using CustomerPortal.Infrastructure.Settings;
using System.Diagnostics;

namespace CustomerPortal.DbContext.Infrastructure
{
    public partial class Net7BoilerplateContext : IdentityDbContext<ApplicationUser, ApplicationRole, long, ApplicationUserClaim, ApplicationUserRole, ApplicationUserLogin, ApplicationRoleClaim, ApplicationUserToken>
    {
        public Net7BoilerplateContext(DbContextOptions<Net7BoilerplateContext> options) : base(options)
        {
        }

        // This Constructor is going to be called every time to register out Context since 
        // I need to set GlobalFilter on my queries. I have taken the idea from here: https://stackoverflow.com/a/66383855/4267429
        // I had no need for new class, as I already have my AppSettings initialization
        public Net7BoilerplateContext(DbContextOptions<Net7BoilerplateContext> options, AppSettings settings) : base(options)
        {
            // Set global filter here
            // OrganizationType = (EOrganisationType)settings.OrganizationType;
        }


        // This method won't be used in the Api project anymore, but it is still used in CronJobs (Background Tasks)
        public static Net7BoilerplateContext Create(string connection)
        {
            var optionsBuilder = new DbContextOptionsBuilder<Net7BoilerplateContext>();
            optionsBuilder.UseSqlServer(connection);
            // optionsBuilder.AddInterceptors(Net7BoilerplateInterceptors.CreateInterceptors());

            // Setup our interceptors
            optionsBuilder.AddInterceptors(BloggingInterceptors.CreateInterceptors());

            // Helps me with debugging stuff
            optionsBuilder.EnableDetailedErrors();
            optionsBuilder.EnableSensitiveDataLogging();
            optionsBuilder.LogTo(message => Debug.WriteLine(message)); // https://learn.microsoft.com/en-us/ef/core/logging-events-diagnostics/simple-logging

            return new Net7BoilerplateContext(optionsBuilder.Options);
        }

        public virtual DbSet<Blog> Blogs { get; set; }
        public virtual DbSet<Post> Posts { get; set; }
        public virtual DbSet<Logging> Logging { get; set; }

        public virtual DbSet<Country> Countries { get; set; }

        public virtual DbSet<OtpVault> OtpVaults { get; set; }

        public virtual DbSet<EmailTemplate> EmailTemplates { get; set; }

        public virtual DbSet<UserRegistrationStep> UserRegistrationSteps { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            { 

                // Setup our interceptors
                optionsBuilder.AddInterceptors(BloggingInterceptors.CreateInterceptors());

                // Helps me with debugging stuff
                optionsBuilder.EnableDetailedErrors();
                optionsBuilder.EnableSensitiveDataLogging();
                optionsBuilder.LogTo(message => Debug.WriteLine(message)); // https://learn.microsoft.com/en-us/ef/core/logging-events-diagnostics/simple-logging
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS"); // override DB annotation if you like
            base.OnModelCreating(modelBuilder); // DO NOT - under any circumstances - REMOVE THIS LINE. This line basically calls our identity constructor first, and without it NOTHING REGARDING IDENTITY WORKS!

            #region Identity stuff
            modelBuilder.Entity<ApplicationUser>()
                .ToTable("Users");

            modelBuilder.Entity<ApplicationRole>().ToTable("Roles");
            modelBuilder.Entity<ApplicationUserRole>().ToTable("UserRoles");
            modelBuilder.Entity<ApplicationUserLogin>().ToTable("UserLogins");
            modelBuilder.Entity<ApplicationUserClaim>().ToTable("UserClaims");
            modelBuilder.Entity<ApplicationRoleClaim>().ToTable("RoleClaims");
            modelBuilder.Entity<ApplicationUserToken>().ToTable("UserTokens");


            #endregion

            modelBuilder.Entity<Country>().ToTable(nameof(Country));
            modelBuilder.Entity<OtpVault>().ToTable(nameof(OtpVault)); 
            modelBuilder.Entity<UserContact>().ToTable("UserContacts");
            modelBuilder.Entity<Product>().ToTable("Products");
            modelBuilder.Entity<UserProductMapping>().ToTable("UserProductMappings");
            modelBuilder.Entity<EmailTemplate>().ToTable(nameof(EmailTemplate));

            modelBuilder.Entity<Logging>(entity =>
            {
                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("Id");
            });

            #region Global filters
            // https://docs.microsoft.com/en-us/ef/core/querying/filters
            // modelBuilder.Entity<Blog>().HasQueryFilter(a => a.PublisherId == PublisherId);
            #endregion

            #region Sequences 
            modelBuilder.HasSequence<long>("BlogSeq")
                .StartsAt(100)
                .IncrementsBy(1)
                .HasMin(100);

            modelBuilder.Entity<Blog>()
                .Property(o => o.Id)
                .HasDefaultValueSql("NEXT VALUE FOR BlogSeq");

            // Sometimes we cannot set the sequence like we did above for the BlogSeq.
            // Reason can be that when scafolding our database it will mark PK of table with "ValueGeneratedNever" 
            // That's why we are going to create "LoggingInterceptor" to help us with inserting data for Logs table. 
            // Another reason for interceptor could be that we have an old system that is still being used while we rework this one. 
            // It could use some obscure logic to fetch next id for Logging record, and we have to reset Sequence before we insert anything. 
            // For more info why I needed this, read the note on top of the file "LoggingInterceptor.cs"
            modelBuilder.HasSequence<long>("LoggingSeq")
                        .StartsAt(2000000)
                        .IncrementsBy(1)
                        .HasMin(2000000);
            #endregion

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
