﻿namespace CustomerPortal.DbContext.Enums
{
    public enum EmailTemplateType
    {
        None = 0,
        OTPVerificationCode = 1,
        WaitingForApproval = 2
    }
}
