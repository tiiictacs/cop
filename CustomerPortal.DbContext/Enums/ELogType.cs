﻿using CustomerPortal.Utility.Attributes;

namespace CustomerPortal.DbContext.Enums
{
    public enum ELogType
    {
        [EnumDescription("Unknown", 0)]
        Unknown = 0,

        [EnumDescription("New blog added", 0)]
        BlogAdded = 1,
        [EnumDescription("Blog updated", 0)]
        BlogUpdated = 2,

        [EnumDescription("New OTP Vault added", 0)]
        OtpVaultAdded = 3,
    }
}
